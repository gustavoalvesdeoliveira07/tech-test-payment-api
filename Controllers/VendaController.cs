using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PaymentAPI.Context;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly AppDbContext _context;

        public VendaController(AppDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistraVenda(string itens, Venda venda)
        {
            venda.IdentificadorPedido = Status.MudaStatus("", "");
            venda.Data = DateTime.Now;

            if (itens == "" || itens == null)
                return BadRequest("Campo itens não pode ser nulo");

            venda.Itens = itens;

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult RecuperaVendaPorId(int id)
        {
            var venda = _context.Vendas.Include(b => b.Vendedor)
            .FirstOrDefault(x => x.VendaId == id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizaVenda(int id, string statusObjetivo)
        {
            var venda = _context.Vendas.Include(b => b.Vendedor).FirstOrDefault(x => x.VendaId == id);

            if (venda == null) 
                return NotFound();

            var statusAtual = venda.IdentificadorPedido;

            try
            {
                venda.IdentificadorPedido = Status.MudaStatus(statusObjetivo, statusAtual);
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            

            _context.Vendas.Update(venda);
            _context.SaveChanges();
            
            return Ok(venda);
        }

    }
}