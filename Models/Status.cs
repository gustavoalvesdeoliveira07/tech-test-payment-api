using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentAPI.Models
{
    public abstract class Status
    {
        private static readonly string[] statusPossiveis = {"Aguardando Pagamento", "Pagamento Aprovado", "Enviado para Transportadora", "Entregue", "Cancelada"};

        public static string MudaStatus(string statusObjetivo, string statusAtual)
        {
            if (statusAtual == "") {
                return statusPossiveis[0];
            }

            var indiceAtual = Array.IndexOf(statusPossiveis, statusAtual);
            var indiceObjetivo = Array.IndexOf(statusPossiveis, statusObjetivo);

            if (indiceObjetivo == -1) throw new ArgumentException($"Status {statusObjetivo} inválido na lista de status disponíveis {statusPossiveis}");

            if ( (indiceObjetivo == (indiceAtual + 1)) || (indiceObjetivo == (statusPossiveis.Length - 1)) )
            {
                return statusPossiveis[indiceObjetivo];
            } else {
                throw new InvalidOperationException($"Alteração de status inválida ({statusAtual}) -> ({statusObjetivo})");
            }
        } 

    }
}