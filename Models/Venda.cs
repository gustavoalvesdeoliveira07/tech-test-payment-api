using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PaymentAPI.Models;


namespace PaymentAPI.Models
{
    public class Venda
    {
        public int VendaId { get; set; }

        public DateTime Data { get; set; }

        public string IdentificadorPedido { get; set; }

        public int VendedorId { get; set; }

        public string Itens { get; set; }

        public Vendedor Vendedor { get; set; }

    }
}